#ifndef Locale_Strings
#define Locale_Strings 1

/* Locale Catalog Source File
 *
 * Automatically created by SimpleCat V3
 * Do NOT edit by hand!
 *
 * SimpleCat �1992-2015 Guido Mersmann
 *
 */



/****************************************************************************/


#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifdef CATCOMP_ARRAY
#undef CATCOMP_NUMBERS
#undef CATCOMP_STRINGS
#define CATCOMP_NUMBERS
#define CATCOMP_STRINGS
#endif

#ifdef CATCOMP_BLOCK
#undef CATCOMP_STRINGS
#define CATCOMP_STRINGS
#endif



/****************************************************************************/


#ifdef CATCOMP_NUMBERS

#define MSG_MENU_TOGGLE 0
#define MSG_TITLE 1

#define CATCOMP_LASTID 1

#endif /* CATCOMP_NUMBERS */


/****************************************************************************/


#ifdef CATCOMP_STRINGS

#define MSG_MENU_TOGGLE_STR "Activate the cycle counter"
#define MSG_TITLE_STR "Cycle counter"

#endif /* CATCOMP_STRINGS */


/****************************************************************************/


#ifdef CATCOMP_ARRAY

struct CatCompArrayType
{
    LONG   cca_ID;
    STRPTR cca_Str;
};

static const struct CatCompArrayType CatCompArray[] =
{
    {MSG_MENU_TOGGLE,(STRPTR)MSG_MENU_TOGGLE_STR},
    {MSG_TITLE,(STRPTR)MSG_TITLE_STR},
};

#endif /* CATCOMP_ARRAY */


/****************************************************************************/


#ifdef CATCOMP_BLOCK

static const char CatCompBlock[] =
{
    "\x00\x00\x00\x00\x00\x1C"
    MSG_MENU_TOGGLE_STR "\x00\x00"
    "\x00\x00\x00\x01\x00\x0E"
    MSG_TITLE_STR "\x00"
};

#endif /* CATCOMP_BLOCK */


/****************************************************************************/



#endif /* Locale_Strings */

