## Version $VER: cyclecounter.acepansion.catalog 1.1 (19.10.2020)
## Languages english fran�ais deutsch espa�ol
## Codeset english 0
## Codeset fran�ais 0
## Codeset deutsch 0
## Codeset espa�ol 0
## SimpleCatConfig CharsPerLine 200
## Header Locale_Strings
## TARGET C english "generated/locale_strings.h" NoCode
## TARGET CATALOG fran�ais "Release/Catalogs/fran�ais/" Optimize
## TARGET CATALOG espa�ol "Release/Catalogs/espa�ol/" Optimize
MSG_MENU_TOGGLE
Activate the cycle counter
Activer le compteur de cycles

Activar el contador de ciclos
;
MSG_TITLE
Cycle counter
Compteur de cycles

Contador de ciclos
;
